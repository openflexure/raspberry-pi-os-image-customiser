### Building qemu-user-static on Rocky

**THIS IS AN INTERNAL NOTE** please don't rely on it, it only been done once!

Install build deps:
```
yum install git glib2-devel libfdt-devel pixman-devel zlib-devel bzip2 ninja-build python3
yum install libaio-devel libcap-ng-devel libiscsi-devel capstone-devel \
                  vte291-devel ncurses-devel \
                 libseccomp-devel nettle-devel libattr-devel libjpeg-devel \
                 brlapi-devel libgcrypt-devel lzo-devel snappy-devel \
                 librdmacm-devel libibverbs-devel cyrus-sasl-devel libpng-devel \
                 libuuid-devel curl-devel libssh-devel \
                 systemtap-sdt-devel libusbx-devel
```
From <https://wiki.qemu.org/Hosts/Linux> 

I then found [another guide](http://logan.tw/posts/2018/02/18/build-qemu-user-static-from-source-code/)

```
Configure QEMU with --static, --disable-system, and --enable-linux-user:
$ ./configure --static --disable-system --enable-linux-user --disable-capstone
```

I needed to further `yum install zlib-static glibc-static glib2-static pcre-static``
Capstone wouldn't link so had to disable it with `--disable-capstone`

After all that, it built :) Link all binaries with

```
for f in *; do if [ -f "$f" ] && [ -x "$f" ]; then echo $f; fi; done

for f in *; do if [ -f "$f" ] && [ -x "$f" ]; then sudo ln -s `pwd`/$f /usr/bin/$f-static; fi; done
```

After all this, I have `qemu-arm-static` that does run some ARM binaries, but `apt` still fails to install the first lot of packages.
