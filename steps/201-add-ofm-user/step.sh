# We need a user for the server - this is defined in global_parameters.sh
source /step/global_parameters.sh

# Create server user, group, path
echo "Adding OF user ${OPENFLEXURE_USER_NAME}"
adduser --system "${OPENFLEXURE_USER_NAME}"
groupadd -f "${OPENFLEXURE_GROUP_NAME}"

usermod -g "${OPENFLEXURE_GROUP_NAME}" "${OPENFLEXURE_USER_NAME}"
usermod -a -G "${OPENFLEXURE_GROUP_NAME}" "${FIRST_USER_NAME}"

# Add openflexure-ws user to required groups
# https://raspberrypi.stackexchange.com/questions/70214/what-does-each-of-the-default-groups-on-the-raspberry-pi-do
# User needs to be in sudo group to enable shutdown and restart API actions
for GRP in adm dialout audio users video plugdev input gpio spi i2c netdev sudo; do
	adduser "$OPENFLEXURE_USER_NAME" "$GRP"
done
# User needs paswordless executable permissions to /usr/sbin/shutdown to enable shutdown and restart API actions
echo "${OPENFLEXURE_USER_NAME} ALL=NOPASSWD: /usr/sbin/shutdown" >> "/etc/sudoers"
