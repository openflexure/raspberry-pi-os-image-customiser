: '
This script registers an avahi service to broadcast the device as being
an available OpenFlexure Microscope. This can be used for discovery of
microscopes connected to the network.
'
apt-get -q install -y avahi-daemon
install -v -m 644 /step/ofm.service "/etc/avahi/services/ofm.service"
