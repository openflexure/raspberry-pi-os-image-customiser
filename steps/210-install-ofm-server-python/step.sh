# Install the Python packages for the OpenFlexure Microscope server

source /step/global_parameters.sh

# Download and install the OFM software

# "Product" information
#disturl="https://build.openflexure.org/openflexure-microscope-server/latest"
# use git for now
server_repo="https://gitlab.com/openflexure/openflexure-microscope-server.git"


# Create the folders
mkdir -v -p "${OFM_VARPATH}"
mkdir -v -p `dirname "${OFM_SERVER}"`  # will hold the server repo


# Create the server folder and clone the git repositories
cd `dirname "$OFM_SERVER"`
git clone $server_repo
pushd openflexure-microscope-server
git checkout v3
popd

#### Create a virtual environment and install the packages
echo -e "\nCreating virtual environment in $OFM_VIRTUALENV"
python3 -m venv "$OFM_VIRTUALENV" --prompt "OFM Server" --system-site-packages

echo "Activating virtualenv..."
# shellcheck disable=SC1090
source "$OFM_VIRTUALENV/bin/activate"

# Work in repo folder
cd "$OFM_SERVER" || exit 1

echo "Using OFM server branch `git rev-parse --abbrev-ref HEAD`" >> /step/log
echo "Using OFM server commit `git rev-parse HEAD`" >> /step/log

pip install --only-binary=:all: -e .[pi]

# Deactivate server environment
deactivate

# Copy the default config file
mkdir -p "/var/openflexure/settings/"
cp "$OFM_SERVER/ofm_config_full.json" "/var/openflexure/settings/ofm_config.json"

# Ensure ownership
chown -R "${OPENFLEXURE_USER_NAME}:${OPENFLEXURE_GROUP_NAME}" /var/openflexure

# Install service
echo "Enabling openflexure-microscope-server service..."

cat > /etc/systemd/system/openflexure-microscope-server.service <<- EOL
[Unit]
Description=Run the OpenFlexure Microscope software using LabThings
After=network.target

[Service]
User=${OPENFLEXURE_USER_NAME}
Group=${OPENFLEXURE_GROUP_NAME}
WorkingDirectory=/var/openflexure
Environment="PATH=$OFM_VIRTUALENV/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin"
ExecStart=$OFM_VIRTUALENV/bin/openflexure-microscope-server -c /var/openflexure/settings/ofm_config.json --host 0.0.0.0 --port 5000 --fallback

[Install]
WantedBy=multi-user.target
EOL

systemctl enable openflexure-microscope-server
