source /step/global_parameters.sh

# Install the SNES gamepad client
apt-get -q install -y libpython3-dev

# Extras
snesclient_giturl="https://gitlab.com/openflexure/openflexure-microscope-snesclient.git"
snesclient_gitbranch="master"

# OpenFlexure SNES Client

# Set available versions of python and pip
snesclient_libpath="`dirname ${OFM_SERVER}`/openflexure-microscope-snesclient"
snesclient_reqsfile="${snesclient_libpath}/requirements.txt"
snesclient_virtualenvdir="${snesclient_libpath}/.venv"

# Clone repo to repo directory
git clone "$snesclient_giturl" "$snesclient_libpath"

# Create virtualenv directory
echo -e "\nCreating virtual environment in $snesclient_virtualenvdir"
python -m venv "$snesclient_virtualenvdir"

echo "Activating virtualenv..."
# shellcheck disable=SC1090
source "$snesclient_virtualenvdir/bin/activate"

# Work in repo folder
cd "$snesclient_libpath" || exit 1

# Checkout branch
git checkout "$snesclient_gitbranch"

# Install requirements
echo -e "Installing additional required Python packages."
"$snesclient_virtualenvdir/bin/pip" -q install wheel
"$snesclient_virtualenvdir/bin/pip" -q install --only-binary=numpy -r "$snesclient_reqsfile"

# Deactivate SNES Client venv
deactivate

# Ensure ownership
chown -R "${OPENFLEXURE_USER_NAME}:${OPENFLEXURE_GROUP_NAME}" ${snesclient_libpath}

# Install service
echo "Enabling openflexure-microscope-snesclient service..."

echo | tee -a  /etc/systemd/system/openflexure-microscope-snesclient.service <<- EOL
[Unit]
Description=Start the OpenFlexure SNES Client looping application
After=network.target

[Service]
User=${OPENFLEXURE_USER_NAME}
Group=${OPENFLEXURE_GROUP_NAME}
WorkingDirectory=/var/openflexure
Environment="PATH=$snesclient_virtualenvdir/bin/"
ExecStart=$snesclient_virtualenvdir/bin/python3 $snesclient_libpath/main.py

[Install]
WantedBy=multi-user.target
EOL

systemctl enable openflexure-microscope-snesclient
