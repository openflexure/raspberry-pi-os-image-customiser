#!/bin/bash -e
source /step/global_parameters.sh

# Install a pre-built Python environment with the microscope client in it
# NB that if we put the virtual environment in the user's folder, it will
# break when the first user gets renamed.
# That's why we now put the venv in /var/openflexure/application.

ofm_client_scripts_dir="/home/${FIRST_USER_NAME}/microscope_python_scripts"
ofm_client_venv="${ofm_varpath}/application/ofm_python_client_venv"

# We'll put this in a folder in the default user's home directory
mkdir -p "${ofm_client_scripts_dir}"
mkdir -p `dirname "${ofm_client_venv}"`

# Make a virtual environment and install the python client
python -m venv "$ofm_client_venv" --prompt "OFM Client"
source "${ofm_client_venv}"/bin/activate
pip install -q --only-binary=:all: \
  labthings_fastapi \
  jupyter notebook matplotlib \
  numpy scipy camera-stage-mapping \
  pysocks
deactivate
ln -s "${ofm_client_venv}" "${ofm_client_scripts_dir}/.venv"

chown -R ${FIRST_USER_NAME} "${ofm_client_scripts_dir}"
chown -R ${FIRST_USER_NAME} "${ofm_client_venv}"