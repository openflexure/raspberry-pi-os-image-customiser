source /step/global_parameters.sh

echo "Installing node for web app"
apt-get -q install -y nodejs npm
echo "Building static web app"
cd "${OFM_SERVER}/webapp/"
for try in {1..3}; do
    echo "Attempting to npm install (attempt $try). Will time out after 5 minutes."
    timeout 5m npm install && break
done
export NODE_OPTIONS=--openssl-legacy-provider
npm run build