# Update config.txt and cmdline.txt to:
# 1. Enable the UART
# 2. Disable the serial console
# 3. Enable the camera (may no longer be needed)
# 4. Up the GPU memory split
patch --directory="/boot" < /step/disable-serial-tty.diff
patch --directory="/boot" < /step/enable-uart-camera-and-gpu.diff
