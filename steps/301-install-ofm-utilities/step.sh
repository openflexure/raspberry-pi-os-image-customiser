source /step/global_parameters.sh

apt-get -q install -y libxslt1-dev proxychains4 python3-socks vim

# This is needed for apt to use a proxy correctly under sudo
cat >> /etc/sudoers.d/010_proxy <<EOF
Defaults env_keep += "all_proxy ALL_PROXY"
EOF

# This pre-configures proxychains for a local socks5 proxy
sed -i 's/^socks4.*9050$/socks5 127.0.0.1 10800/g' /etc/proxychains4.conf

# Add useful aliases to bashrc
cat >> /home/pi/.bashrc <<'EOF'

# Useful commands when SSHing in with '-R 10800'
export-proxy () {
        export http_proxy="$1"
        export https_proxy="$1"
        export all_proxy="$1"
        export HTTP_PROXY="$1"
        export HTTPS_PROXY="$1"
        export ALL_PROXY="$1"
}
run_without_proxy () {
        http_proxy="" https_proxy="" all_proxy="" HTTP_PROXY="" HTTPS_PROXY="" ALL_PROXY="" $@
}
alias export-ssh-proxy="export-proxy socks5h://127.0.0.1:10800"
alias clear-proxy="export-proxy ''"
alias pc="run_without_proxy proxychains"
alias sudo-pc="run_without_proxy sudo proxychains"

EOF

# Add a commented-out SSH config to use the proxy
ssh_folder="/home/${FIRST_USER_NAME}/.ssh/"
install -d -o "${FIRST_USER_NAME}" -g "${FIRST_USER_NAME}" -m 700 "${ssh_folder}"
cat >> "${ssh_folder}/config" <<EOF
# Uncomment the line below to use SSH via a reverse proxy
# made using 'ssh user@microscope -R 10800'
#ProxyCommand nc -X 5 -x 127.0.0.1:10800 %h %p
EOF
chown -R "${FIRST_USER_NAME}" "${ssh_folder}"
