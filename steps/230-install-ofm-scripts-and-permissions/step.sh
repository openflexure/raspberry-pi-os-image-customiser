source /step/global_parameters.sh

chown -R "${OPENFLEXURE_USER_NAME}:${OPENFLEXURE_GROUP_NAME}" "${OFM_VARPATH}"
chmod -R g+w "${OFM_VARPATH}"

tempdir=`mktemp -d`
pushd "$tempdir"
wget https://gitlab.com/openflexure/openflexure-microscope-cli/-/raw/master/ofm
wget https://gitlab.com/openflexure/openflexure-microscope-cli/-/raw/master/ofm-init
install -v -m 755 ofm-init "/usr/bin/ofm-init"
install -v -m 755 ofm "/usr/bin/ofm"
popd
rm -rf "$tempdir"

echo "source /usr/bin/ofm-init" >> "/home/${FIRST_USER_NAME}/.bashrc"
echo "source /usr/bin/ofm-init" >> "/root/.bashrc"
