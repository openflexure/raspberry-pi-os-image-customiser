: '
This script copies over two network manager connection profiles for the wired
interface. These produce behaviour similar to traditional dhcpd autoconfig, i.e.
it will try DHCP for 5 seconds, twice, then use link-local IPv4 and IPv6 addresses
'
install -v -m 600 -o root /step/wired_auto.nmconnection "/etc/NetworkManager/system-connections/wired_auto.nmconnection"
install -v -m 600 -o root /step/wired_linklocal.nmconnection "/etc/NetworkManager/system-connections/wired_linklocal.nmconnection"
