# Make it possible for the openflexure server to mount/unmount USB disks

mkdir -p /mnt/openflexure-data
cat <<EOF >> /etc/fstab
/dev/sda1       /mnt/openflexure-data   vfat    rw,user,noauto,fmask=113,dmask=002,uid=openflexure-ws,gid=openflexure-ws        0     0
EOF
