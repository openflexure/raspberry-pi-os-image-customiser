source /step/global_parameters.sh

apt-get -q install -y libvips

cd `dirname ${OFM_SERVER}`
git clone https://gitlab.com/openflexure/openflexure-stitching.git

pushd openflexure-stitching
python --version
python -m venv .venv --prompt "OFM Stitching"
source .venv/bin/activate
python -m pip install -q --upgrade pip
pip install -q --only-binary=:all: -e .
ls .venv/bin

echo "Using OFM stitching branch `git rev-parse --abbrev-ref HEAD`" >> /step/log
echo "Using OFM stitching commit `git rev-parse HEAD`" >> /step/log

popd
