OPENFLEXURE_USER_NAME=openflexure-ws
OPENFLEXURE_GROUP_NAME=${OPENFLEXURE_USER_NAME}
FIRST_USER_NAME=pi

# Export important variables
OFM_VARPATH="/var/openflexure"
OFM_SERVER="${OFM_VARPATH}/application/openflexure-microscope-server"
OFM_VIRTUALENV="${OFM_SERVER}/.venv"