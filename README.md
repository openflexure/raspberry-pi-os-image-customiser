# OpenFlexure Raspberry Pi OS Customiser

There are two previous repositories that use `pi-gen` to build OpenFlexure's SD card image. Recently, neither has worked reliably on the systems I have access to - and besides, it seems inefficient to build the entire operating system just so we can then install our software on top of it. This repo is loosely inspired by `pi-gen` but is much, much more basic. Simply put, we:

1. Download a Raspberry Pi OS image
2. Add some free space and mount it
3. Run scripts (a bit like pi-gen) to install packages and configure thee system
4. Unmount it

After this, we have a customised OpenFlexure image, in a fraction of the time it would have taken had we built it from scratch.

## Usage

It should be as simple as
```
sudo ./build.sh
```
Root is needed to mount the filesystem and to set ownership correctly, I'm not sure it's really possible without this. 
If you get errors related to binary formats, you may need to enable binfmt-misc transparent emulation. If you've already installed `qemu-user-static` and `binfmt-support` this may be as simple as `sudo update-binfmts --enable qemu-arm`.
We now use `qcow2` images, which allows copy-on-write for massively less time spent copying over base images. This will require `qemu-nbd` and `qemu-utils` on your system.

By default, we build everything in the `work/` directory. If this is empty or missing, we start fresh, from the base image (which is downloaded if required from Raspberry Pi's webserver). If the work directory exists, we will skip all steps that have completed successfully (by checking they saved a snapshot QCOW2 image). This means that if a stage fails, you can fix the script and re-run the build process. Note that the script is **not** smart enough to detect when scripts have changed and re-run them: for that, you'd need to delete the snapshot by hand.

## Dependencies

These have recently changed - the `yum` dependencies won't include the right `qemu` utilities
```
apt install cloud-guest-utils qemu-user-static qemu-utils qemu-nbd kpartx
```
or
```
yum install cloud-utils-growpart qemu-img
```
You need qemu-user-static either way, but on Rocky I currently use a Docker-based solution as there's no native package in `yum`. You must enable this (after every reboot) with the script at `utilities/enable_qemu.sh`.

I've used `growpart` to simplify growing the partition; this is found in `cloud-guest-utils` on Debian and `cloud-utils-growpart` on Rocky et al.
