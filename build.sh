#!/bin/bash
# OpenFlexure Raspberry Pi OS customiser
# This script is loosely based on pi-gen, but starts from an existing 
# Raspberry Pi OS image.
# For now, it needs to be run as root to get permissions right - I would
# dearly love to tighten this up, but our current solution is to be very
# careful about what we merge.

set -e

#BASE_IMAGE_URL="https://downloads.raspberrypi.com/raspios_lite_arm64/images/raspios_lite_arm64-2023-12-11/2023-12-11-raspios-bookworm-arm64-lite.img.xz"
#BASE_IMAGE_URL="https://downloads.raspberrypi.com/raspios_lite_armhf/images/raspios_lite_armhf-2023-12-11/2023-12-11-raspios-bookworm-armhf-lite.img.xz"
BASE_IMAGE_URL="https://downloads.raspberrypi.com/raspios_lite_armhf/images/raspios_lite_armhf-2024-07-04/2024-07-04-raspios-bookworm-armhf-lite.img.xz"

source global_parameters.sh

BASE_IMAGE_NAME_XZ=`basename "${BASE_IMAGE_URL}"`
BASE_IMAGE_NAME="${BASE_IMAGE_NAME_XZ%.xz}"
DATE=`date --rfc-3339=date`
if [[ $BASE_IMAGE_NAME =~ raspios-([a-z]+)-(armhf|arm64)-(lite|full).img ]]; then
    release=${BASH_REMATCH[1]}
    variant=${BASH_REMATCH[2]}
    arch=${BASH_REMATCH[3]}
    IMAGE_PREFIX="${DATE}-raspios-openflexure-${release}-${variant}-${arch}"
    echo "Images will be prefixed with '$IMAGE_PREFIX'."
else
    echo "Could not match base image name to find distribution/architecture"
    exit 1
fi
CACHE_DIR="`pwd`/cache"
OUTPUT_DIR="`pwd`/dist"

source utilities/image_handling.sh


BASE_IMAGE_DIR="${CACHE_DIR}/base_images"
function download_base_image () {
    pushd "${BASE_IMAGE_DIR}"
    # Start by downloading and uncompressing the disk image, if needed
    if [ ! -e "${BASE_IMAGE_NAME}" ]; then
        echo "${BASE_IMAGE_NAME} not present, will attempt to obtain it"
        if [ ! -e ${BASE_IMAGE_NAME_XZ} ]; then
            echo "${BASE_IMAGE_NAME_XZ} not present, will attempt to download it"
            wget "${BASE_IMAGE_URL}"
        fi
        echo "Uncompressing... ${BASE_IMAGE_NAME_XZ}"
        xz -d "${BASE_IMAGE_NAME_XZ}"
    fi
    popd
}

build_all_steps() {
    # First, download the image if needed, and initialise the work directory
    if [ ! -e "${BASE_IMAGE}" ]; then
        mkdir -p "${BASE_IMAGE_DIR}"
        pushd "${BASE_IMAGE_DIR}"
        # Start by downloading and uncompressing the disk image, if needed
        download_base_image
        initialise_with_base_image "${BASE_IMAGE_NAME}"
        popd
    else
        echo "resuming work on current image"
    fi

    STEPS="${STEPS:-steps/*/step.sh}"

    last_snapshot="base_image"
    allow_skipping=true
    for step_script in $STEPS; do
        step_dir=`dirname "${step_script}"`
        step_name=`basename ${step_dir}`

        if $allow_skipping; then
            if [ -e "${WORKDIR}/${step_name}.qcow2" ]; then
                echo "A snapshot exists for ${step_name}, skipping."
                last_snapshot="${step_name}"
                continue
            fi
        fi
        allow_skipping=false # We can only skip initial steps, not subsequent ones

        echo "Running ${step_name}"
        mount_working_image "${last_snapshot}" "${step_dir}"
        trap umount_trap EXIT
        #df -h /dev/nbd0p2
        chroot "${ROOTFS_DIR}/" bash -e /step/step.sh
        #df -h /dev/nbd0p2
        umount_working_image
        trap "" EXIT

        create_snapshot "${step_name}"
        last_snapshot="${step_name}"

        if [ -e "${step_dir}/EXPORT" ]; then
            make_bootable_image "${step_name}" "${IMAGE_PREFIX}"
        fi

        echo "Finished $step_name"
    done

    copy_image_log "${IMAGE_PREFIX}"

    echo "Finished all scripts."
    echo ""
    echo "Will now compress images with xz. This may take ~30 minutes per image."
    echo "It's completely safe to cancel at any time with Ctrl + C."

    for image in dist/${IMAGE_PREFIX}/*.img; do
        xz -v "${image}"
    done

    echo "xz-compressed images are now ready for upload. Build complete :)"
}

clean() {
    rm -rf "${WORKDIR}"
    rm -rf "${OUTPUT_DIR}"
}

mount_cli() {
    if [ -f "$1" ]; then
        echo "basename $1"
        image=`basename $1`
        step_name="${image%.*}"
    else
        step_name="$1"
    fi
    if [ -d $2 ]; then
        step_dir="$2"
    else
        step_dir=`pwd`
    fi
    if [ -e "$WORKDIR/$step_name.qcow2" ]; then
        echo "Mounting step $step_name"
        mount_working_image $step_name $step_dir
        echo "Success"
    else
        echo "Usage: $0 mount <step_name> [<step_dir>]"
        echo "<step_dir> will be mounted as /step"
        echo "Sensible steps will be listed below:"
        ls "$WORKDIR" | sed 's/\.qcow2$//'
        exit 1
    fi
}

case $1 in
    "clean")
        clean
        ;;

    "mount")
        mount_cli $2 $3
        echo "Step $2 has been copied to $WORKING_IMAGE and mounted at $ROOTFS_DIR"
        ;;

    "umount")
        set +e
        umount_working_image
        echo "The image has been unmounted"
        ;;

    "chroot")
        mount_cli $2 $3
        trap umount_trap EXIT
        chroot "${ROOTFS_DIR}/" bash
        umount_working_image
        trap "" EXIT
        ;;

    *)
        build_all_steps
        ;;
esac
