#!/bin/bash

on_chroot() {
	# if ! mount | grep -q "$(realpath "${ROOTFS_DIR}"/proc)"; then
	# 	mount -t proc proc "${ROOTFS_DIR}/proc"
	# fi

	# if ! mount | grep -q "$(realpath "${ROOTFS_DIR}"/dev)"; then
	# 	mount --bind /dev "${ROOTFS_DIR}/dev"
	# fi
	
	# if ! mount | grep -q "$(realpath "${ROOTFS_DIR}"/dev/pts)"; then
	# 	mount --bind /dev/pts "${ROOTFS_DIR}/dev/pts"
	# fi

	# if ! mount | grep -q "$(realpath "${ROOTFS_DIR}"/sys)"; then
	# 	mount --bind /sys "${ROOTFS_DIR}/sys"
	# fi

	#setarch linux32 capsh $CAPSH_ARG "--chroot=${ROOTFS_DIR}/" -- -e "$@"
    chroot "${ROOTFS_DIR}/" bash -e
}
export -f on_chroot