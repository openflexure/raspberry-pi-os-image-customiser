#!/bin/bash
set -e
echo "Clearing binfmt_misc register..."
sudo bash -c "echo -1 >  /proc/sys/fs/binfmt_misc/status"
sleep 0.5
echo "Registering qemu-arm-static from binfmt_misc..."
sudo  docker run --privileged --rm tonistiigi/binfmt --install arm
echo "Done. `qemu-arm-static` will not be visible, but ARM binaries should now run."
