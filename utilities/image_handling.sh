### This block of code deals with manipulating the qcow2 images
# These variables set where we keep the images while it's work-in-progress
WORKDIR="`pwd`/work"
BASE_IMAGE="${WORKDIR}/base_image.qcow2"
WORKING_IMAGE="${WORKDIR}/working_image.qcow2"
WORKING_LOG="${WORKDIR}/working_log"
DEVICE=/dev/nbd0
# These variables set the mount points
ROOTFS_DIR="/mnt/raspbian"
BOOTFS_DIR="${ROOTFS_DIR}/boot"
PIP_CACHE_DIR="${CACHE_DIR}/pip"
APT_CACHE_DIR="${CACHE_DIR}/apt"
APT_CACHE_MOUNTPOINT="${ROOTFS_DIR}/var/cache/apt"
PIP_CACHE_MOUNTPOINT="${ROOTFS_DIR}/root/.cache/pip"
STEP_MOUNTPOINT="${ROOTFS_DIR}/step"
GLOBAL_PARAMETERS_FILE="`pwd`/global_parameters.sh"
GLOBAL_PARAMETERS_MOUNTPOINT="${STEP_MOUNTPOINT}/global_parameters.sh"
LOG_MOUNTPOINT="${STEP_MOUNTPOINT}/log"

function image_log () {
    echo "$1" >> "${WORKDIR}/image_log"
    echo "IMAGE: $1"
}

function create_snapshot () {
    # create a snapshot of the current working image
    # by moving it to a different filename.
    # A new working image will be created when needed.
    mv "${WORKING_IMAGE}" "${WORKDIR}/$1.qcow2"
    if [ -f "${WORKING_LOG}" ]; then
        if [ -s "${WORKING_LOG}" ]; then
            image_log "*** Step log from $1 ***"
            cat "${WORKING_LOG}" >> "${WORKDIR}/image_log"
            image_log "*** End of step log from $1 ***"
        else
            image_log "No log from step $1"
        fi
        mv "${WORKING_LOG}" "${WORKDIR}/$1.log"
    fi
    image_log "created snapshot $1"
}

function initialise_with_base_image () {
    # convert an input image file, and empty the work directory
    echo "Cleaning work directory and starting again from $1"
    rm -rf "${WORKDIR}"
    mkdir -p "${WORKDIR}"
    qemu-img convert -O qcow2 "$1" "${BASE_IMAGE}"
    image_log "base_image from $1"
    modprobe nbd max_part=8  # ensure nbd is loaded - needed to mount images
    qemu-img resize "${BASE_IMAGE}" 8G
    image_log "resized image to 8Gb"
    qemu-nbd --connect="${DEVICE}" "${BASE_IMAGE}"
    growpart "${DEVICE}" 2
    resize2fs "${DEVICE}p2"
    qemu-nbd --disconnect "${DEVICE}"
    image_log "resized root partition"
}

function create_and_map_working_image() {
    # Create a new copy-on-write working image, then map to a block device
    # arguments:
    # $1: the snapshot on which to base the working image
    # Create a new working image, based on the argument to this function
    qemu-img create -f qcow2 -b "$1.qcow2" -F qcow2 "${WORKING_IMAGE}" 8G

    # Set up a block device for the image
    qemu-nbd --connect="${DEVICE}" "${WORKING_IMAGE}"
    BOOT_DEVICE="${DEVICE}p1"
    ROOT_DEVICE="${DEVICE}p2"
    # It can take a moment for the devices to appear, wait up to 0.5s
    for try in {1..5}; do
        if [ -e "${ROOT_DEVICE}" ]; then
            break
        else
            sleep 0.1
        fi
    done
}

function mount_working_image () {
    # Use qemu-nbd to map the working image to a block device, then mount it.
    # arguments:
    # $1: the snapshot on which to base the working image
    # $2: (optional) a directory to mount as /step
    create_and_map_working_image $1

    # Mount the device
    mkdir -p "${ROOTFS_DIR}"
    mount -t ext4 "${ROOT_DEVICE}" "${ROOTFS_DIR}"
    mkdir -p "${BOOTFS_DIR}"
    mount -t vfat "${BOOT_DEVICE}" "${BOOTFS_DIR}"
    echo "Mounted at ${ROOTFS_DIR} and ${BOOTFS_DIR}"
    mkdir -p "${APT_CACHE_MOUNTPOINT}"
    mkdir -p "${PIP_CACHE_MOUNTPOINT}"
    mkdir -p "${APT_CACHE_DIR}"
    mkdir -p "${PIP_CACHE_DIR}"
    mount --bind "${APT_CACHE_DIR}" "${APT_CACHE_MOUNTPOINT}"
    mount --bind "${PIP_CACHE_DIR}" "${PIP_CACHE_MOUNTPOINT}"
    if [ -d "$2" ]; then
        echo "Mounting $2 as /step"
        mkdir -p "${STEP_MOUNTPOINT}"
        mount --bind "$2" "${STEP_MOUNTPOINT}"
        touch "${GLOBAL_PARAMETERS_MOUNTPOINT}"
        mount --bind -o ro "${GLOBAL_PARAMETERS_FILE}" "${GLOBAL_PARAMETERS_MOUNTPOINT}"
        echo -n "" > "${WORKING_LOG}"
        touch "${LOG_MOUNTPOINT}"
        echo mount --bind "${WORKING_LOG}" "${LOG_MOUNTPOINT}"
        mount --bind "${WORKING_LOG}" "${LOG_MOUNTPOINT}"
    fi
}

function unmap_working_image() {
    qemu-nbd --disconnect "${DEVICE}"
}

function umount_working_image () {
    echo "Unmounting image"
    # The step mountpoint is optional, so don't fail if it's not mounted (hence || true)
    umount "${GLOBAL_PARAMETERS_MOUNTPOINT}" || true
    if [ ! -s "${GLOBAL_PARAMETERS_MOUNTPOINT}" ]; then
        rm "${GLOBAL_PARAMETERS_MOUNTPOINT}" || true
    fi
    umount "${LOG_MOUNTPOINT}" || true
    if [ ! -s "${LOG_MOUNTPOINT}" ]; then
        rm "${LOG_MOUNTPOINT}" || true
    fi
    umount "${STEP_MOUNTPOINT}" || true
    umount "${PIP_CACHE_MOUNTPOINT}"
    umount "${APT_CACHE_MOUNTPOINT}"
    umount "${BOOTFS_DIR}"
    sleep 0.5
    umount "${ROOTFS_DIR}"
    sync
    unmap_working_image
}

function umount_trap () {
    echo "Received EXIT signal, unmounting..."
    set +e
    umount_working_image
    exit $?
}

# based on: https://github.com/SirLagz/RaspberryPi-ImgAutoSizer
# pinched from Raspberry Pi's pi-gen 
# https://gitlab.com/openflexure/pi-gen/-/blob/openflexure-armhf/scripts/qcow2_handling
function resize_qcow2() {
	# ROOT_MARGIN=$((800*1024*1024))
	ROOT_MARGIN=$((1*1024*1024))
	PARTED_OUT=`parted -s -m "${DEVICE}" unit B print`
	PART_NO=`echo "$PARTED_OUT" | grep ext4 | awk -F: ' { print $1 } '`
	PART_START=`echo "$PARTED_OUT" | grep ext4 | awk -F: ' { print substr($2,1,length($2)-1) } '`

	e2fsck -y -f "${DEVICE}p2" || true

	DATA_SIZE=`resize2fs -P "${DEVICE}p2" | awk -F': ' ' { print $2 } '`
	BLOCK_SIZE=$(dumpe2fs -h "${DEVICE}p2" | grep 'Block size' | awk -F': ' ' { print $2 }')
	BLOCK_SIZE=${BLOCK_SIZE// /}

	let DATA_SIZE=$DATA_SIZE+$ROOT_MARGIN/$BLOCK_SIZE
	resize2fs -p "${DEVICE}p2" $DATA_SIZE
	sleep 1

	let PART_NEW_SIZE=$DATA_SIZE*$BLOCK_SIZE
	let PART_NEW_END=$PART_START+$PART_NEW_SIZE
	ACT1=`parted -s "${DEVICE}" rm 2`
	ACT2=`parted -s "${DEVICE}" unit B mkpart primary $PART_START $PART_NEW_END`
	NEW_IMG_SIZE=`parted -s -m "${DEVICE}" unit B print free | tail -1 | awk -F: ' { print substr($2,1,length($2)-1) } '`
}

# This function is pinched from pi-gen as well
# create raw img from qcow2: make_bootable_image <in.qcow2> <out.img>
function make_bootable_image() {
    EXPORT_IMAGE="${OUTPUT_DIR}/${2:+$2/}${2:+$2-}${1}.img"
    mkdir -p "${OUTPUT_DIR}${2:+/$2}"
    # Generate a bootable image, based on $1, which is a snapshot.
	image_log "Attempting to export bootable image based on $1"
    create_and_map_working_image "$1"
    image_log "Resizing filesystem"
	resize_qcow2
	sync
	unmap_working_image

	if [ -z "$NEW_IMG_SIZE" ]; then
		image_log "NEW_IMG_SIZE could not be calculated, cannot process image. Exit."
		exit 1
    else
        image_log "Image is now $NEW_IMG_SIZE."
	fi

	image_log "Shrinking qcow2 image"
	qemu-img resize --shrink "${WORKING_IMAGE}" $NEW_IMG_SIZE
	sync

	image_log "Convert qcow2 to raw image"
	qemu-img convert -f qcow2 -O raw "${WORKING_IMAGE}" "${EXPORT_IMAGE}"
	sync

	echo "Get PARTUUIDs from image"
	IMGID="$(blkid -o value -s PTUUID "${EXPORT_IMAGE}")"

	BOOT_PARTUUID="${IMGID}-01"
	echo "Boot: $BOOT_PARTUUID"
	ROOT_PARTUUID="${IMGID}-02"
	echo "Root: $ROOT_PARTUUID"

	echo "Mount image"
	MOUNTROOT="${ROOTFS_DIR}"
	mkdir -p $MOUNTROOT

	MOUNTPT="${ROOTFS_DIR}"
	PARTITION=2
	mount "${EXPORT_IMAGE}" "$MOUNTPT" -o loop,offset=$[ `/sbin/sfdisk -d "${EXPORT_IMAGE}" | grep "start=" | head -n $PARTITION | tail -n1 | sed 's/.*start=[ ]*//' | sed 's/,.*//'` * 512 ],sizelimit=$[ `/sbin/sfdisk -d "${EXPORT_IMAGE}" | grep "start=" | head -n $PARTITION | tail -n1 | sed 's/.*size=[ ]*//' | sed 's/,.*//'` * 512 ] || exit 1

	MOUNTPT="${BOOTFS_DIR}"
	PARTITION=1
	mount "${EXPORT_IMAGE}" "$MOUNTPT" -o loop,offset=$[ `/sbin/sfdisk -d "${EXPORT_IMAGE}" | grep "start=" | head -n $PARTITION | tail -n1 | sed 's/.*start=[ ]*//' | sed 's/,.*//'` * 512 ],sizelimit=$[ `/sbin/sfdisk -d "${EXPORT_IMAGE}" | grep "start=" | head -n $PARTITION | tail -n1 | sed 's/.*size=[ ]*//' | sed 's/,.*//'` * 512 ] || exit 1

	if [ ! -d "${MOUNTROOT}/root" ]; then
		image_log "Image damaged or not mounted. Exit."
		exit 1
	fi

	echo "Setup PARTUUIDs"
    # NB the ROOTDEV and BOOTDEV placeholders have already been substituted - so
    # I used a different regexp.
	if [ ! -z "$BOOT_PARTUUID" ] && [ ! -z "$ROOT_PARTUUID" ]; then
		image_log "Set UUIDs to make it bootable"
		sed -i "s/PARTUUID=.*-01 /PARTUUID=${BOOT_PARTUUID} /" "${ROOTFS_DIR}/etc/fstab"
		sed -i "s/PARTUUID=.*-02 /PARTUUID=${ROOT_PARTUUID} /" "${ROOTFS_DIR}/etc/fstab"
		sed -i "s/PARTUUID=.*-02 /PARTUUID=${ROOT_PARTUUID} /" "${BOOTFS_DIR}/cmdline.txt"
	fi

    image_log "Exported bootable image $EXPORT_IMAGE"

	echo "Umount image"
	sync
	umount "${BOOTFS_DIR}" || exit 1
	umount "${ROOTFS_DIR}" || exit 1

	echo "Remove qcow2 export image"
	rm -f "${WORKING_IMAGE}"
}

function copy_image_log() {
    EXPORT_LOG="${OUTPUT_DIR}/${1}/${1}-image-log.log"
    mkdir -p "${OUTPUT_DIR}/${1}"
    cp "${WORKDIR}/image_log" "${EXPORT_LOG}"
}